import React, { Component } from 'react';
import LoginForm from '../forms/LoginForm';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  submit = (data) => {
    console.log(data);
  }

  render() {
    return (
      <div>
        <h1>Login Page</h1>

        <LoginForm submit={this.submit} />
      </div>
    );
  }
}

export default LoginPage;
